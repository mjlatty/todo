function notes(_, __, context) {
    return context.prisma.note.findMany();
}

module.exports = {
    notes,
}