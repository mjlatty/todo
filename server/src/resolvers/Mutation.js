const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../utils')

async function signup(parent, args, context, info) {
    const password = await bcrypt.hash(args.password, 10)
    const user = await context.prisma.user.create({ data: { ...args, password } })
    const token = jwt.sign({ userId: user.id }, APP_SECRET)

    return {
        token,
        user,
    }
}

async function login(parent, args, context, info) {
    const user = await context.prisma.user.findOne({ where: { email: args.email } })
    if (!user) {
        throw new Error('No such user found')
    }

    const valid = await bcrypt.compare(args.password, user.password)
    if (!valid) {
        throw new Error("Invalid password")
    }

    const token = jwt.sign({ userId: user.id }, APP_SECRET)

    return {
        token,
        user,
    }
}

function addNote(_, { text }, context) {
    //const userId = getUserId(context)
    const userId = 1;

    const newNote = context.prisma.note.create({
        data: {
            text: text,
            author: { connect: { id: userId } },
        }
    });
    return {
        success: true,
        message: "Note added successfully",
        note: newNote
    };
}

function deleteNote(_, { id }, context, info) {
    const userId = 1;
    console.log(id);
    const deletedNote = context.prisma.note.delete({ where: { id: parseInt(id) } });
    return {
        success: true,
        message: "Note deleted successfully",
        note: deletedNote
    }

}

module.exports = {
    signup,
    login,
    addNote,
    deleteNote,
}