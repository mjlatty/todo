const { ApolloServer } = require('apollo-server')
const typeDefs = require('./schema')
const { PrismaClient } = require('@prisma/client')
const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')
const User = require('./resolvers/User')
const Note = require('./resolvers/Note')

const resolvers = {
    Query,
    Mutation,
    User,
    Note,
}

const prisma = new PrismaClient()

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: req => ({
        ...req,
        prisma,
    }),
});

server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});