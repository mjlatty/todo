const { gql } = require('apollo-server');

// TODO might be cool to track revisions to notes - you could look at note history
// array of last edit times?
// TODO allow creation of "task" notes? YAGNI
// topicTags: [String]!
// peopleTags: [String]!
const typeDefs = gql`
  type Note {
      id: ID!
      text: String!
      author: User
  }

  type AuthPayload {
      token: String
      user: User
  }

  type User {
      id: ID!
      name: String!
      email: String!
      notes: [Note]!
  }

  type Query {
      notes: [Note!]!
      note(id: ID!): Note
      me: User
  }

  type Mutation {
      addNote(text: String!): NoteUpdateResponse!
      editNote(id: ID!): NoteUpdateResponse!
      deleteNote(id: ID!): NoteUpdateResponse!
      signup(email: String!, password: String!, name: String!): AuthPayload
      login(email: String!, password: String!): AuthPayload
  }

  type NoteUpdateResponse {
      success: Boolean!
      message: String
      note: Note
  }
`;

module.exports = typeDefs;