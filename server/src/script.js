const { PrismaClient } = require("@prisma/client")

const prisma = new PrismaClient()

async function main() {
    const newNote = await prisma.note.create({
        data: {
            text: "Adding my first jot to the database!",
        },
    })

    const allNotes = await prisma.note.findMany();
    console.log(allNotes)
}

main()
    .catch(e => {
        throw e
    })
    .finally(async () => {
        await prisma.disconnect()
    })