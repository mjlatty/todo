import React from "react";
import PropTypes from "prop-types";
import Note from "./Note";
import AddNote from "./AddNote";
import { Grid, makeStyles, Card, CardContent, Typography, Container } from "@material-ui/core";
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { GET_NOTES } from '../queries';

const useStyles = makeStyles((theme) => ({
  notesList: {
    backgroundColor: theme.palette.primary.main,
    margin: theme.spacing(1),
  },
  title: {
    color: theme.palette.common.white,
  }
}));

const Thread = ({ toggleNote }) => {
  const { loading, error, data } = useQuery(GET_NOTES);
  const classes = useStyles();

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  return (
    <>
      <Container className={classes.notesList}>
        {data.notes.length > 0 ?
          <>
            <Typography className={classes.title} variant="h6" noWrap>
              Thread Title Placeholder
            </Typography>
            <CardContent>
              {data.notes.map(({ id, text }) => (
                <Note key={id} id={id} text={text} onClick={() => toggleNote(id)} />
              ))}
            </CardContent>
          </>
          :
          <Typography className={classes.title} variant="h6" noWrap>
            No notes entered in this thread yet
          </Typography>}
      </Container>
      <AddNote />
    </>
  );
};


Thread.propTypes = {
  //toggleNote: PropTypes.func.isRequired,
};

export default Thread;
