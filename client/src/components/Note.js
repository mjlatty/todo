import React from "react";
import PropTypes from "prop-types";
import {
  Grid,
  Card,
  Checkbox,
  makeStyles,
  IconButton,
  CardContent,
} from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import { findByLabelText } from "@testing-library/react";
import gql from 'graphql-tag';
import { useMutation } from '@apollo/react-hooks';
import { GET_NOTES } from '../queries';
import { DELETE_NOTE } from '../mutations';

const Note = ({ id, text }) => {
  const useStyles = makeStyles((theme) => ({
    noteCard: {
      flexGrow: 1,
      margin: theme.spacing(1),
      padding: theme.spacing(1),
    },
    deleteButton: {
      float: "right",
    }
  }));

  const classes = useStyles();

  const [deleteNoteMutation] = useMutation(DELETE_NOTE);

  const deleteNote = () => {
    deleteNoteMutation({
      variables: { id: id },
      update: (store, { data: { deleteNote } }) => {
        const data = store.readQuery({
          query: GET_NOTES
        });
        store.writeQuery({
          query: GET_NOTES,
          data: {
            notes: data.notes.filter(
              note => note.id !== deleteNote.note.id)
          }
        });
      },
    });
  }

  return (
    <div>
      <Card className={classes.noteCard}>
        <span>{text}</span>
        <IconButton className={classes.deleteButton} aria-label="delete" onClick={deleteNote}>
          <ClearIcon />
        </IconButton>
      </Card>
    </div>
  );
};

Note.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

export default Note;
