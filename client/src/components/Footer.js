import React from "react";
import {
  Container,
  Card,
  CardContent,
  makeStyles
} from "@material-ui/core";
import AddNote from "./AddNote";


const Footer = () => {
  const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.secondary.light,
    },
  }));

  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <AddNote />
      </CardContent>
    </Card>
  );
};

export default Footer;
