import React, { useState } from "react";
import { connect } from "react-redux";
import { TextField, makeStyles, Card, CardContent } from "@material-ui/core";
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { GET_NOTES } from '../queries';
import { ADD_NOTE } from '../mutations';

const AddNote = () => {
  const useStyles = makeStyles((theme) => ({
    noteTextField: {
      width: "100%",
      //backgroundColor: theme.palette.background.paper,
    },
  }));

  const classes = useStyles();

  const [newNoteText, setNewNoteText] = useState("");

  const [addNoteMutation] = useMutation(ADD_NOTE);

  const handleKeyUp = (event) => {
    if (event.keyCode == 13) {
      event.preventDefault();
      setNewNoteText(newNoteText.trim());
      if (!newNoteText) {
        return;
      }
      addNoteMutation({
        variables: { text: newNoteText },
        update: (store, { data: { addNote } }) => {
          const data = store.readQuery({
            query: GET_NOTES
          });
          store.writeQuery({
            query: GET_NOTES,
            data: {
              notes: [...data.notes, addNote.note]
            }
          });
        },
      });
      setNewNoteText("");
    }
  }

  const handleChange = (e) => {
    setNewNoteText(e.target.value);
  }

  return (
    <Card className={classes.root}>
      <CardContent>
        <TextField
          className={classes.noteTextField}
          value={newNoteText}
          id="outlined-multiline-flexible"
          label="Add Jot"
          multiline
          rowsMax={10}
          onChange={handleChange}
          onKeyUp={handleKeyUp}
          variant="outlined"
        ></TextField>
      </CardContent>
    </Card>
  );
};

export default AddNote;
