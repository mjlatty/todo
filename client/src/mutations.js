import gql from 'graphql-tag';

export const ADD_NOTE = gql`
  mutation addNote($text: String!) {
    addNote(text: $text) {
      note {
        id
        text
      }
    }
  }
`;

export const DELETE_NOTE = gql`
  mutation deleteNote($id: ID!) {
    deleteNote(id: $id) {
      note {
        id
      }
    }
  }
`;