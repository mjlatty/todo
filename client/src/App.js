import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import PropTypes from "prop-types";
import {
  Container,
  Grid,
  makeStyles,
  Tab,
  Tabs,
  Box,
  Button,
  Card,
  CardContent,
  CardActions,
  Typography,
  useTheme
} from "@material-ui/core";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Thread from "./components/Thread";
import ThreadsSidebar from "./components/ThreadsSidebar";
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Z_FIXED } from "zlib";
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import PostAddIcon from '@material-ui/icons/PostAdd';
import FilterListIcon from '@material-ui/icons/FilterList';
import EditIcon from '@material-ui/icons/Edit';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    width: "100%",
    height: "100vh",
    flexDirection: "column",
    flexWrap: "no-wrap",
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appHeader: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    backgroundColor: theme.palette.primary.main,
    flexGrow: 1,
    padding: theme.spacing(3),

  },
  appFooter: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    position: "fixed",
    bottom: "0",
  },
  appBody: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
  },
}));

const App = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const threads = [
    { title: 'Putting', jotCount: 17 },
    { title: 'React', jotCount: 97 },
    { title: 'Job Search', jotCount: 43 },
    { title: 'Meraiah', jotCount: 12 },
  ];

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <Typography className={classes.title} variant="h6" noWrap>
        Threads
      </Typography>
      <Divider />
      <IconButton aria-label="delete" className={classes.margin}><EditIcon fontSize="large" /></IconButton>
      <IconButton aria-label="delete" className={classes.margin}><FilterListIcon fontSize="large" /></IconButton>
      <IconButton aria-label="delete" className={classes.margin}><PostAddIcon fontSize="large" /></IconButton>
      <Divider />
      <List>
        {threads.map(({ title, jotCount }, index) => (
          <ListItem button key={title}>
            <ListItemText
              primary={title}
              secondary="Jan 7, 2014"
            />
            <span className={'MuiLabel-amount'}>{jotCount}</span>
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <div className={classes.appHeader}>
        <Header />
      </div>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.appBody}>
          <Thread />
        </div>
      </main>
    </div>
  );
}

const CardTitle = () => { };

export default App;
