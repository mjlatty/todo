# Migration `20200714075539-initial-migration`

This migration has been generated by Micah Latty at 7/14/2020, 7:55:39 AM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE TABLE "public"."Note" (
"authorId" integer   ,
"createdAt" timestamp(3)  NOT NULL DEFAULT CURRENT_TIMESTAMP,
"id" SERIAL,
"text" text  NOT NULL ,
    PRIMARY KEY ("id"))

CREATE TABLE "public"."User" (
"email" text  NOT NULL ,
"id" SERIAL,
"name" text  NOT NULL ,
"password" text  NOT NULL ,
    PRIMARY KEY ("id"))

CREATE UNIQUE INDEX "User.email" ON "public"."User"("email")

ALTER TABLE "public"."Note" ADD FOREIGN KEY ("authorId")REFERENCES "public"."User"("id") ON DELETE SET NULL  ON UPDATE CASCADE
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration ..20200714075539-initial-migration
--- datamodel.dml
+++ datamodel.dml
@@ -1,0 +1,27 @@
+// 1
+datasource db {
+    provider = "postgresql"
+    url = "***"
+}
+
+// 2
+generator client {
+    provider = "prisma-client-js"
+}
+
+// 3
+model Note {
+    id        Int      @id @default(autoincrement())
+    createdAt DateTime @default(now())
+    text      String
+    author    User?    @relation(fields: [authorId], references: [id])
+    authorId  Int?
+}
+
+model User {
+    id       Int    @id @default(autoincrement())
+    name     String
+    email    String @unique
+    password String
+    notes    Note[]
+}
```


